from tango import DbDatum, AttrDataFormat, CmdArgType, DispLevel, AttrQuality, DevState


class GenericDataClass:
    def __init__(self, dictionary):
        for k, v in dictionary.items():
            setattr(self, k, v)


class MockCommandInfo(GenericDataClass):
    pass


class MockDeviceAttributeConfig(GenericDataClass):
    pass


class MockDeviceInfo(GenericDataClass):
    pass


class MockDatabaseDeviceInfo(GenericDataClass):
    pass


class MockDB(object):

    def get_device_exported(self, pattern):
        device_pattern = DbDatum(pattern)
        device_pattern.value_string.append(pattern)
        return device_pattern

    def get_device_domain(self, pattern):
        return ["sys"]

    def get_device_family(self, url):
        return ["tg_test"]

    def get_device_info(self, url):
        result = MockDatabaseDeviceInfo({'class_name': 'sys/tg_test/1', '_class ': 'some enchanted class',
                                         'server': 'sys', 'pid': 12345, 'started_date': '1/1/2001',
                                         'stopped_date': 'Active', 'exported': False})
        return result

    def get_device_member(self, pattern):
        device_member = DbDatum(pattern)
        device_member.value_string.append('1')
        return device_member

    def get_device_server_name_list(self, url):
        return ["server"]

    def get_device_property_list(self, name, pattern):
        return ["a pile of mince"]

    def get_device_attribute_list(self, name, pattern):
        return ["a pile of mince"]


class MockProxy(object):

    def __init__(self):
        self.attrib_dict = {'data_format': AttrDataFormat.SCALAR, 'data_type': CmdArgType.DevDouble,
                            'disp_level': DispLevel.OPERATOR, 'value': 200,
                            'format': '%6.2f', 'label': 'ampli', 'max_dim_x': 1, 'max_dim_y': 0,
                            'name': 'ampli', 'writable': True, 'unit': '', 'description': '',
                            'min_value': 0, 'max_value': 0, 'min_alarm': 0, 'max_alarm': 0, 'w_value': 0,
                            'quality': AttrQuality.ATTR_VALID}

        self.mockAttribConfig = MockDeviceAttributeConfig(self.attrib_dict)

    def attribute_query(self, name):
        return self.mockAttribConfig

    def attribute_list_query(self):
        result = [self.mockAttribConfig]
        return result

    def command_list_query(self):
        result = [
            MockCommandInfo({'cmd_name': 'DevBoolean', 'disp_level': DispLevel.OPERATOR,
                             'in_type': CmdArgType.DevBoolean, 'out_type': CmdArgType.DevBoolean,
                             'cmd_tag': 0, 'in_type_desc': 'Any boolean value',
                             'out_type_desc': 'Echo of the argin value'})
        ]
        return result

    def name(self):
        return "sys/tg_test/1"

    async def state(self):
        return DevState.ON

    def alias(self):
        return ""

    def info(self):
        result = MockDeviceInfo({'dev_class': 'TangoTest',
                                 'doc_url': 'http://www.esrf.eu/computing/cs/tango/tango_doc/ds_doc/',
                                 'server_host': 'buster', 'server_id': 'TangoTest/test', 'server_version': 5})
        return result

    async def read_attribute(self, name, extract_as):
        return self.mockAttribConfig

    def command_inout(self, command, argin):
        return

    def write_read_attribute(self, name, value):
        return value


class MockDeviceProxyCache(object):
    def get(self, name):
        return MockProxy()
