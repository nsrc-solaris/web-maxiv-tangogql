# importing the requests library
import timeit
import requests

REQUESTS_PER_SET = 1000
REPEATS = 20

# api-endpoint
URL = "http://localhost:5004/db"
BASIC_COMMAND = {'query': 'mutation {  executeCommand(device: "sys/tg_test/1", command: "DevBoolean", argin: 1) '
                          '{   ok    message   output }}', 'variables': None}
BASIC_QUERY = {'query': 'query {devices(pattern: "sys/tg_test/1"){state}} '
                        '{\n    ok\n    message\n    output\n  }\n}\n', 'variables': None}

ATTRIB_QUERY = {'query': '''query {   devices(pattern: "sys/tg_test/1") {     attributes(pattern: "ampli") {
      name device datatype dataformat writable label unit description displevel value writevalue  quality      
      minvalue  maxvalue  minalarm  maxalarm  }}}''' 
                         '{\n    ok\n    message\n    output\n  }\n}\n', 'variables': None}

ATTRIB_SUBSCRIBE = {'query':  '''subscription { attributes(fullNames: ["sys/tg_test/1/double_scalar"])
    { attribute device fullName value writeValue quality timestamp }}'''
                              '{\n    ok\n    message\n    output\n  }\n}\n', 'variables': None}


def send_request (json):
    # sending get request and saving the response as response object
    requests.post(url=URL, json=json)


def time_batch_of_requests(label, json):
    request = f'send_request({json})'
    print(label, timeit.repeat(repeat=REPEATS, number=REQUESTS_PER_SET,
                               stmt=f'send_request({json})',
                               setup='from __main__ import  send_request'))


time_batch_of_requests("Query_State", BASIC_QUERY)
time_batch_of_requests("Device Command", BASIC_COMMAND)
time_batch_of_requests("Attribute Query", ATTRIB_QUERY)
time_batch_of_requests("Attribute Subscribe", ATTRIB_SUBSCRIBE)
