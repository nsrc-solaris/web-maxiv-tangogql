import json
import os


# TODO: required_groups should be renamed in order not to make clear that:
# - only one group needs to match
# - an empty list means no restrictions (does this make sense?)


class Config:
    def __init__(self):

        try:
            self.secret = os.environ["TANGOGQL_SECRET"]
        except KeyError:
            raise ConfigError("no secret provided")

        try:
            required_groups = json.loads(os.getenv("TANGOGQL_REQUIRED_GROUPS", "[]"))
        except ValueError:
            raise ConfigError("required_groups must be list")

        if not all(isinstance(group, str) for group in required_groups):
            raise ConfigError("required_groups must consist of strings")

        self.required_groups = required_groups

        self.app_bind = os.environ.get("TANGOGQL_BIND", "0.0.0.0")
        self.app_port = int(os.environ.get("TANGOGQL_PORT", "5004"))
        self.debug_mode = bool(int(os.environ.get("TANGOGQL_DEBUG", "0")))


class ConfigError(Exception):
    def __init__(self, reason):
        super().__init__(f"Config error: {reason}")
